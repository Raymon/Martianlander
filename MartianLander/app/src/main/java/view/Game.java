package view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ProgressBar;

import com.example.MartianLander.R;

import model.Rocket;
import model.Terrain;


public class Game extends SurfaceView implements SurfaceHolder.Callback {
    private Activity activity;
    private Terrain terrain;
    private Rocket rocket;
    private GameThread gameThread;
    private ProgressBar fuleProgressBar;
    private Paint paint = new Paint();
    private boolean leftThrusterActive = false;
    private boolean rightThrusterActive = false;
    private boolean mainThrusterActive = false;

    private int maxFuel = 50;
    private double gravity = 0.2;
    private double increaseSpeed = 1;
    private double decreaseSpeed = -1.5;
    private int sideDe = 1;
    private int mainDe = 2;
    private int skyColor = Color.rgb(244,150,124);
    private SoundPool soundPool;
    private int crashSoundId;
    private int successSoundId;
    private int fireSoundId;
    private boolean playSound = true;

    public Game(Context context) {
        this(context, null);
    }

    public Game(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        gameThread = new GameThread(this);
        soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        crashSoundId = soundPool.load(this.getContext(), R.raw.boom, 1);
        successSoundId = soundPool.load(this.getContext(), R.raw.success, 1);
        fireSoundId = soundPool.load(this.getContext(), R.raw.fire, 1);
    }

    public void setModel(Activity activity, Rocket rocket, Terrain terrain) {
        this.activity = activity;
        this.rocket = rocket;
        this.terrain = terrain;
    }

    public void controlProgress(ProgressBar fuelBar) {
        this.fuleProgressBar = fuelBar;
        this.fuleProgressBar.setBackgroundColor(skyColor);
        this.fuleProgressBar.setProgress(maxFuel);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        gameThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        boolean retry = true;

        while (retry) {
            try {
                gameThread.join();
                retry = false;
            }
            catch (InterruptedException e) {
            }
        }
    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

    public void startGame() {
        gameThread.setRunning(true);
    }
    public void stopGame() {
        gameThread.setRunning(false);
    }

    /**
     * restart game
     */
    public void restart() {
        Rocket rocket = new Rocket(getContext());
        Terrain terrainModel = new Terrain(activity);
        this.setModel(activity, rocket, terrainModel );
        playSound = true;
    }

    public void moveLeft() {
        if(!rocket.isFuelFinished()){
            rocket.setRocketSpeedX(increaseSpeed);
            rocket.reduceFuel(sideDe);
            soundPool.play(fireSoundId,1.0f, 1.0f, 0, 0, 1.0f);
            leftThrusterActive = true;
        }
    }

    public void moveRight() {
        if(!rocket.isFuelFinished()){
            rocket.setRocketSpeedX(decreaseSpeed);
            rocket.reduceFuel(sideDe);
            soundPool.play(fireSoundId,1.0f, 1.0f, 0, 0, 1.0f);
            rightThrusterActive = true;
        }
    }

    public void moveUp() {
        if(!rocket.isFuelFinished()){
            rocket.setRocketSpeedY(decreaseSpeed);
            rocket.reduceFuel(mainDe);
            soundPool.play(fireSoundId,1.0f, 1.0f, 0, 0, 1.0f);
            mainThrusterActive = true;
        }
    }
    //endregion

    //region Drawing logic code here

    public void drawAnimation(Canvas canvas) {
        canvas.drawColor(skyColor);
        terrain.drawRocket(canvas, paint);
        rocket.setRocketSpeedY(gravity);

        if (landed())
        {
            if(crashed()){
                drawCrashed(canvas);
                playCrashSound();
            }else{
                playSuccessSound();
                drawUnCrashed(canvas);
            }
        }else {
            updatePosition(canvas);
        }

        rocket.getRocketBottomPosition();
        fuleProgressBar.setProgress(rocket.getFuel());
        if(rightThrusterActive){
            rocket.drawRightFire(canvas, paint);

            rightThrusterActive = false;
        }
        if(mainThrusterActive){
            rocket.drawMainFire(canvas, paint);
            soundPool.play(fireSoundId,1.0f, 1.0f, 0, 0, 1.0f);
            mainThrusterActive = false;
        }
        if(leftThrusterActive){
            rocket.drawLeftFire(canvas, paint);
            soundPool.play(fireSoundId,1.0f, 1.0f, 0, 0, 1.0f);
            leftThrusterActive = false;
        }
    }
    private void playSuccessSound(){
        if (playSound)
        {
            soundPool.play(successSoundId, 1.0f, 1.0f, 0, 0, 1.0f);
            playSound = false;
        }
    }
    private void playCrashSound(){
        if (playSound)
        {
            soundPool.play(crashSoundId, 1.0f, 1.0f, 0, 0, 1.0f);
            playSound = false;
        }
    }

    /**
     * draw Rocket
     * @param canvas
     */
    public void drawUnCrashed(Canvas canvas) {
        rocket.drawRocket(canvas, paint);
    }

    /**
     * draw crashed
     * draw crater
     * @param canvas
     */
    public void drawCrashed(Canvas canvas){
        terrain.drawCrater(rocket.getMainThruster_PosX(), rocket.getMainThruster_PosY(), canvas, paint);
        rocket.drawCrashRocket(canvas, paint);
    }

    /**
     * update Craft Position
     * add speed to update the positon
     * @param canvas
     */
    public void updatePosition(Canvas canvas){

        rocket.setRocketStartPosition_Y(rocket.getRocketStartPosition_Y() + (int) rocket.getRocketSpeedY());
        rocket.setRocketStartPosition_X(rocket.getRocketStartPosition_X() + (int) rocket.getRocketSpeedX());
        rocket.drawRocket(canvas, paint);
    }

    public boolean landed(){
        return (
                IsTouchSurface(rocket.getLeftThruster_PosX(), rocket.getLeftThruster_PosY()) ||
                        IsTouchSurface(rocket.getRightThruster_PosX(), rocket.getRightThruster_PosY()) ||
                        IsTouchSurface(rocket.getMainThruster_PosX(), rocket.getMainThruster_PosY())
        );
    }

    public boolean crashed(){
        return !(
                IsTouchSurface( rocket.getLeftThruster_PosX(), rocket.getLeftThruster_PosY()) &&
                        IsTouchSurface(rocket.getRightThruster_PosX(), rocket.getRightThruster_PosY()) &&
                        IsTouchSurface(rocket.getMainThruster_PosX(), rocket.getMainThruster_PosY())
        );
    }

    public boolean IsTouchSurface(double point_X, double point_Y) {
        int[] terrain_X = terrain.getTerrain_X();
        int[] terrain_Y = terrain.getTerrain_Y();
        int interferes = 0;

        for (int i = 0; i < terrain_X.length - 1; i++) {
            int x1 = terrain_X[i];
            int x2 = terrain_X[i + 1];
            int y1 = terrain_Y[i];
            int y2 = terrain_Y[i + 1];
            double slope = 0;
            if (x2 - x1 != 0) {
                slope = (double) (y2 - y1) / (x2 - x1);
            }
            boolean isOnline = (point_Y < slope * (point_X - x1) + y1);
            boolean isOnTwoPoints = ((x1 <= point_X) && (point_X < x2)) || ((x2 <= point_X) && (point_X < x1));
            if ((isOnTwoPoints) && isOnline) {
                interferes++;
            }
        }
        return (interferes % 2 != 0);
    }

}
