
package controller;

import android.app.Activity;
import android.view.View;
import android.widget.Button;

import com.example.MartianLander.R;

import model.Rocket;
import view.Game;

public class Buttons {

    private Game game;
    private Button btLeft, btUp, btRight, btRetry;

    public Buttons(Activity activity, Rocket model, Game game) {
        this.game = game;
        btLeft = (Button) activity.findViewById(R.id.button_left);
        btUp = (Button) activity.findViewById(R.id.button_up);
        btRight = (Button) activity.findViewById(R.id.button_right);
        btRetry = (Button) activity.findViewById(R.id.btnRestart) ;
        addButtonListeners();
    }

    private void addButtonListeners() {
        btUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                game.moveUp();
            }
        });
        btLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                game.moveLeft();
            }
        });
        btRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                game.moveRight();
            }
        });
        btRetry.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
               game.restart();
            }
        });
    }
}