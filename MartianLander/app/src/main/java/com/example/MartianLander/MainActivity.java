package com.example.MartianLander;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ProgressBar;

import controller.Buttons;
import model.Rocket;
import model.Terrain;
import view.Game;

public class MainActivity extends Activity {
    private Game game;
    ProgressBar fuelBar;
    Buttons buttons;

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main_layout);
        Terrain terrainModel = new Terrain(this);
        Rocket rocket = new Rocket(this);
        fuelBar = (ProgressBar)findViewById(R.id.fuelProgressBar);
        game = (Game)findViewById(R.id.gameView);
        game.controlProgress(fuelBar);	//initial the progress bar


        game.setModel(this, rocket, terrainModel);
        buttons = new Buttons(this, rocket, game);
    }

    @Override
    protected void onStart() {
        super.onStart();
        game.startGame();
    }
    @Override
    protected void onPause() {
        super.onPause();
        game.stopGame();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        game.startGame();
    }

    @Override
    protected void onResume() {
        super.onResume();
        game.startGame();
    }


}
