package model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.MartianLander.R;

public class Rocket {

    //region fields
    private int fuel = 50;

    private Resources resources;
    private Bitmap rocketBitmap;
    private Bitmap sideThrusterBitmap;
    private Bitmap mainThrusterBitmap;
    private Bitmap crashBitmap;
    //rocket and thrusters position, start speed, size
    private float rocketStartSpeedX = 0;
    private float rocketStartSpeedY = 0;
    private float rocketStartPosition_X = 380;
    private float rocketStartPosition_Y = 100;
    private float LeftThruster_PosX;
    private float LeftThruster_PosY;
    private float RightThruster_PosX;
    private float RightThruster_PosY;

    private float MainThruster_PosX;
    private float MainThruster_PosY;

    private int RocketSize = 100;
    private int mainThrusterSize = 45;
    private int sideThrusterSize = 30;

    /**
     * Constructor, Require context to retrive bitmap from resources.
     * @param context
     */
    public Rocket(Context context){
        this.resources = context.getResources();  //get resources from context
        loadBitmaps(); //load the bitmap and set size
        getRocketBottomPosition(); //get the positon of the bitmaps
    }

    public void loadBitmaps(){
        rocketBitmap = retriveBitmap(R.drawable.rocket, RocketSize);
        mainThrusterBitmap = retriveBitmap(R.drawable.main, mainThrusterSize);
        sideThrusterBitmap = retriveBitmap(R.drawable.small, sideThrusterSize);
        crashBitmap = retriveBitmap(R.drawable.crash, RocketSize);
    };

    private Bitmap retriveBitmap(int res , int size) {
        return Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, res), size, size, true);
    }

    private int getBitmapWidth(Bitmap bitmap) {
        return bitmap.getWidth();
    }

    private int getBitmapHeight(Bitmap bitmap) {
        return bitmap.getHeight();
    }

    public void drawRocket(Canvas canvas, Paint paint){
        resetWhenOutScreen(canvas);
        canvas.drawBitmap(rocketBitmap, rocketStartPosition_X - getBitmapWidth(rocketBitmap)/2, rocketStartPosition_Y, paint);
    }
    public void drawMainFire(Canvas canvas, Paint paint){
        canvas.drawBitmap(mainThrusterBitmap, MainThruster_PosX -getBitmapWidth(mainThrusterBitmap)/2, MainThruster_PosY -10, paint);
    }
    public void drawLeftFire(Canvas canvas, Paint paint){
        canvas.drawBitmap(sideThrusterBitmap, LeftThruster_PosX -10, LeftThruster_PosY -10, paint);
    }
    public void drawRightFire(Canvas canvas, Paint paint){
        canvas.drawBitmap(sideThrusterBitmap, RightThruster_PosX +10 - getBitmapWidth(sideThrusterBitmap), RightThruster_PosY -10, paint);
    }
    public void drawCrashRocket(Canvas canvas, Paint paint) {
        canvas.drawBitmap(crashBitmap, rocketStartPosition_X - getBitmapWidth(crashBitmap)/2, rocketStartPosition_Y, paint);
    }
    public float getRocketStartPosition_X() {
        return rocketStartPosition_X;
    }

    public void setRocketStartPosition_X(float rocketStartPosition_X) {
        this.rocketStartPosition_X = rocketStartPosition_X;
    }

    public float getRocketStartPosition_Y() {
        return rocketStartPosition_Y;
    }

    public void setRocketStartPosition_Y(float rocketStartPosition_Y) {
        this.rocketStartPosition_Y = rocketStartPosition_Y;
    }

    public float getLeftThruster_PosX() {
        return LeftThruster_PosX;
    }

    public float getLeftThruster_PosY() {
        return LeftThruster_PosY;
    }

    public float getRightThruster_PosX() {
        return RightThruster_PosX;
    }

    public float getRightThruster_PosY() {
        return RightThruster_PosY;
    }

    public float getMainThruster_PosX() {
        return MainThruster_PosX;
    }

    public float getMainThruster_PosY() {
        return MainThruster_PosY;
    }

    public float getRocketSpeedX(){
        return rocketStartSpeedX;
    }

    public void setRocketSpeedX(double decreaseSpeed) {
        rocketStartSpeedX += decreaseSpeed;
    }

    public float getRocketSpeedY(){
        return rocketStartSpeedY;
    }

    public void setRocketSpeedY(double decreaseSpeed) {
        rocketStartSpeedY += decreaseSpeed;
    }

    /**
     * set 0 when rocket position out of screen
     */
    public void resetWhenOutScreen(Canvas canvas){
        if(rocketStartPosition_X >= canvas.getWidth()){
            rocketStartPosition_X = 0;
        }else if(rocketStartPosition_X <= 0){
            rocketStartPosition_X = canvas.getWidth();
        }
    }
    /**
     * get the positon of the rocket
     */
    public void getRocketBottomPosition(){
        MainThruster_PosX = rocketStartPosition_X;
        MainThruster_PosY = rocketStartPosition_Y + getBitmapHeight(rocketBitmap);
        LeftThruster_PosX = rocketStartPosition_X - getBitmapWidth(rocketBitmap)/2;
        LeftThruster_PosY = rocketStartPosition_Y + getBitmapHeight(rocketBitmap);
        RightThruster_PosX = rocketStartPosition_X + getBitmapWidth(rocketBitmap)/2;
        RightThruster_PosY = rocketStartPosition_Y + getBitmapHeight(rocketBitmap);
    }
    /**
     * @return  the number of fuel
     */
    public int getFuel(){
        return fuel;
    }

    /**
     * minus number of fuel
     * @param     oneFuel .
     */
    public void reduceFuel(int oneFuel){
        fuel -= oneFuel;
    }

    /**
     * Returns a boolean to show if fuel finished
     * @return    true if fuel finished, else false.
     */
    public boolean isFuelFinished(){
        if(fuel <= 0)
            return true;
        else
            return false;
        }
}
