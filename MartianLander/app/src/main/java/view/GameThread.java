package view;

import android.graphics.Canvas;
import android.view.SurfaceHolder;


public class GameThread extends Thread {

    //--- Fields ---
    private int refreshRate = 10;
    private Game game;
    private SurfaceHolder mHolder;
    private boolean mRunning = false;

    //--- Constructor ---
    public GameThread(Game game) {
        super();
        this.game = game;
        this.mHolder = game.getHolder();
    }

    /**
     * Method to control the animation run status
     * @param running  Defines if animation should run or not
     */
    public void setRunning(boolean running) {
        this.mRunning = running;
    }
    @Override
    public void run() {
        super.run();
        Canvas canvas;
        while (mRunning) {
            canvas = null;
            try {
                canvas = mHolder.lockCanvas();
                synchronized (mHolder) {
                    game.drawAnimation(canvas);
                }
            }
            finally {
                if (canvas != null) {
                    mHolder.unlockCanvasAndPost(canvas);
                }
            }
            try {
                Thread.sleep(refreshRate);
            } catch (Exception e) {
            }
        }
    }
}
