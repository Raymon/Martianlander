package model;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.example.MartianLander.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Terrain {

    private Resources resource;
    private Context context;
    private Path path = new Path();
    private ArrayList<Integer> temp_X = new ArrayList<Integer>();
    private ArrayList<Integer> temp_Y = new ArrayList<Integer>();

    private int actualWidth;
    private int actualHeight;
    public int Terrain_X[];
    public int Terrain_Y[];
    //Constructor
    public Terrain(Context context){
        this.context = context;
        this.resource = context.getResources() ;
        makeRandomTerrain();
    }
    public void getTerrainSquare(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        actualWidth = displayMetrics.widthPixels;
        actualHeight = displayMetrics.heightPixels;
    }
    public int[] getTerrain_X(){
        return Terrain_X;
    }

    public int[] getTerrain_Y(){
        return Terrain_Y;
    }



    /**
     * make random terrain
     */
    private void makeRandomTerrain() {
        getTerrainSquare();
        Random random = new Random();
        for(int i =0; i < 50 ; i ++){
            int anInt = random.nextInt(actualWidth);
            temp_X.add(anInt);
        }
        Collections.sort(temp_X, Collections.reverseOrder());

        for(int j=0; j< 50; j++){
            int anInt = random.nextInt(actualHeight/20);
            temp_Y.add((actualHeight -400 + anInt));
        }
        int startInt = random.nextInt(35);
        int temp = temp_Y.get(startInt).intValue();
        for(int x=startInt; x< startInt+15; x++){
            temp_Y.set(x, temp);
        }
        Terrain_X = new int[50];
        Terrain_Y = new int[50];

        for (int i = 3; i < 48; i++) {
            Terrain_X[i] = temp_X.get(i).intValue();
            Terrain_Y[i] = temp_Y.get(i).intValue();
        }
        Terrain_X[0] = 0;
        Terrain_Y[0] = actualHeight;
        Terrain_X[1] = actualWidth;
        Terrain_Y[1] = actualHeight;
        Terrain_X[2] = actualWidth;
        Terrain_Y[2] = actualHeight - 500;
        Terrain_X[48] = 0;
        Terrain_Y[48] = actualHeight - 500;
        Terrain_X[49] = 0;
        Terrain_Y[49] = actualHeight;
        for (int i = 0; i < 50; i++) {
            path.lineTo(Terrain_X[i], Terrain_Y[i]);
        }
    }

    /**
     * drawRocket rocket.
     */
    public void drawRocket(Canvas canvas, Paint paint) {
        Bitmap bitmap = BitmapFactory.decodeResource(resource, R.drawable.mars);
        BitmapShader mBitmapShader = new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        paint.setShader(mBitmapShader);
        canvas.drawPath(path, paint);
    }

    /**
     * drawRocket Crater when rocket crash.
     */
    public void drawCrater(float x, float y, Canvas canvas, Paint paint){
        paint.setColor(Color.rgb(244,150,124));
        paint.setShader(null);
        canvas.drawCircle(x, y-50, 80, paint);
    }


}
